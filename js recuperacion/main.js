var iarbol = new Arbol();

iarbol.nodoPadre = iarbol.agregarNodo("21", 21, null, 0)

iarbol.nodoPadre.Ni = iarbol.agregarNodo("5", 5, iarbol.nodoPadre, 1); 
iarbol.nodoPadre.Ni.Ni = iarbol.agregarNodo("1", 1, iarbol.nodoPadre.Ni, 2);
iarbol.nodoPadre.Ni.Ni.Ni = iarbol.agregarNodo("0", 0, iarbol.nodoPadre.Ni.Ni, 3); 
iarbol.nodoPadre.Ni.Ni.Nd = iarbol.agregarNodo("4", 4, iarbol.nodoPadre.Ni.Ni, 3);
iarbol.nodoPadre.Ni.Nd = iarbol.agregarNodo("7", 7, iarbol.nodoPadre.Ni, 2); 
iarbol.nodoPadre.Ni.Nd.Ni = iarbol.agregarNodo("6", 6, iarbol.nodoPadre.Ni.Ni, 3);
iarbol.nodoPadre.Ni.Nd.Nd = iarbol.agregarNodo("15", 15, iarbol.nodoPadre.Ni.Ni, 3);

iarbol.nodoPadre.Nd = iarbol.agregarNodo("78", 78, iarbol.nodoPadre, 1); 
iarbol.nodoPadre.Nd.Ni = iarbol.agregarNodo("45", 45, iarbol.nodoPadre.Nd, 2);
iarbol.nodoPadre.Nd.Ni.Ni = iarbol.agregarNodo("0", 0, iarbol.nodoPadre.Nd.Ni, 3);
iarbol.nodoPadre.Nd.Ni.Nd = iarbol.agregarNodo("77", 77, iarbol.nodoPadre.Nd.Ni, 3);
iarbol.nodoPadre.Nd.Nd = iarbol.agregarNodo("90", 90, iarbol.nodoPadre.Nd, 2);
iarbol.nodoPadre.Nd.Nd.Ni = iarbol.agregarNodo("80", 80, iarbol.nodoPadre.Nd.Ni, 3);
iarbol.nodoPadre.Nd.Nd.Nd = iarbol.agregarNodo("99", 99, iarbol.nodoPadre.Nd.Ni, 3);

console.log(iarbol);
iarbol.VHijos(iarbol.nodoPadre);

var z =  iarbol.bValor(iarbol.nodoPadre);
console.log(z);

var caminoarbol = iarbol.bCamino(z);
console.log(caminoarbol);

var sum = iarbol.SumarCaminoN(z);
console.log(sum);